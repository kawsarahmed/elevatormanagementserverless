﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Threading.Tasks;

namespace TestAzureStorage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=teststorager3;AccountKey=p9fqmQABvbWic4JLSJbb1ZHg2GOcDvHWSHrg1hfu7iBmTrWSElJf5i5tGQCmPD1pUFR0oHvAsfNmhlQiNjfG6Q==;EndpointSuffix=core.windows.net";
            var tableName = "testtbl1";

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);
       
            PersonEntity person = new PersonEntity("ruhul", "amin")
             { Email="ruhul@selise.ch"};
           
            AddNewPerson(cloudTable, person).Wait();


            // Console.ReadKey();
        }

        public static async Task AddNewPerson(CloudTable cloudTable,PersonEntity personEntity)
        {
            TableOperation tableOperation = TableOperation.InsertOrMerge(personEntity);
            TableResult tableResult = await cloudTable.ExecuteAsync(tableOperation);

            PersonEntity insertedPerson = tableResult.Result as PersonEntity;
            Console.WriteLine("Person Added Succesfully");
        }

        public class PersonEntity: TableEntity
        {
            public PersonEntity() { }
            public PersonEntity(string firstName, string lastName)
            {
                PartitionKey = firstName;
                RowKey = lastName;
                Timestamp = DateTime.UtcNow;
            }
            public string Email { get; set; }



        }


    }
}
