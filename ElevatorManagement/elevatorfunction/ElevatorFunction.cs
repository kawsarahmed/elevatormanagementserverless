using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Net.Http;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ElevatorFunction
{
    public static class ElevatorFunction
    {
        private static HttpClient httpClient = new HttpClient();

        private static string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=elevatormanagement;AccountKey=4EGFQYP2xBCHvIuOpw0TH5DsTSYt+5oAEgV427w/SwB+f0s/vwiyDqYxh7+UsIsMMEDGg8D+iJVg0rImq959qA==;EndpointSuffix=core.windows.net";
        private static string tableName = "elevatormanagement";



        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            string responseMessage = string.IsNullOrEmpty(name)
                ? "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response."
                : $"Hello, {name}. This HTTP triggered function executed successfully.";


             return new OkObjectResult(responseMessage);
        }



        public static async Task AddNewPerson(CloudTable cloudTable, PersonEntity personEntity)
        {
            TableOperation tableOperation = TableOperation.InsertOrMerge(personEntity);
            TableResult tableResult = await cloudTable.ExecuteAsync(tableOperation);

            PersonEntity insertedPerson = tableResult.Result as PersonEntity;
            Console.WriteLine("Person Added Succesfully");
        }
   


        [FunctionName("index")]
        public static IActionResult GetHomePage([HttpTrigger(AuthorizationLevel.Anonymous)] HttpRequest req, ExecutionContext context)
        {
            var path = Path.Combine(context.FunctionAppDirectory, "content", "index.html");
            return new ContentResult
            {
                Content = File.ReadAllText(path),
                ContentType = "text/html",
            };
        }

        [FunctionName("negotiate")]
        public static SignalRConnectionInfo Negotiate(
            [HttpTrigger(AuthorizationLevel.Anonymous)] HttpRequest req,
            [SignalRConnectionInfo(HubName = "serverless")] SignalRConnectionInfo connectionInfo)
        {
            return connectionInfo;
        }


        //[FunctionName("broadcast")]
        //public static async Task Broadcast([TimerTrigger("*/25 * * * * *")] TimerInfo myTimer,
        //[SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        //{            
        //    await signalRMessages.AddAsync(
        //        new SignalRMessage
        //        {
        //            Target = "newMessage",
        //            Arguments = new[] { $"Signal time is : {DateTime.Now.ToString()}" }
        //        });
        //}

        public class PersonEntity : TableEntity
        {
            public PersonEntity() { }
            public PersonEntity(string firstName, string lastName)
            {
                PartitionKey = firstName;
                RowKey = lastName;
                Timestamp = DateTime.UtcNow;
            }    
            public string BuildingId { get; set; }
            public string FromFloorId { get; set; }
            public string ToFloorId { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime? UpdatedDate { get; set; }
            public string OperationStatus { get; set; }
            public string UpdateOperationId { get; set; }
            public string OperatorId { get; set; }
            public string CreatedBy { get; set; }
            public string ElevatorStatus { get; set; }
            public string Direction{ get; set; }
        }
        public class OperatorView
        {
            public string BuildingId { get; set; }
            public string FromFloorId { get; set; }
            public int Count { get; set; }         
            public string OperatorId { get; set; }          
            public string ElevatorStatus { get; set; }
            public string OperatorStatus { get; set; }
            public List<string> ToFloorIds { get; set; }
            public string Direction { get; set; }
        }       



         [FunctionName("messages")]         
        public static Task SendMessage(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
            [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);          
           
            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());           
 
            PersonEntity person = new PersonEntity( Guid.NewGuid().ToString(), Guid.NewGuid().ToString())
            {
              BuildingId= data?.BuildingId,                
              FromFloorId= data?.FromFloorId,
              ToFloorId= data?.ToFloorId,
              CreatedDate=DateTime.UtcNow,
              OperationStatus= "Pending",
              CreatedBy= "b7c2c5f4-d593-4187-ae02-bbac8cc41e4f",
              ElevatorStatus="",
              OperatorId="",
              Direction=data?.Direction

            };

            AddNewPerson(cloudTable, person).Wait();           

            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where( "OperationStatus eq 'pending'");

           string tableObject =JsonConvert.SerializeObject( cloudTable.ExecuteQuery(query));          
           var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);


            var signalAfterDataCreated= new
            {              
                message = "DataCreated"
            };

            return signalRMessages.AddAsync(
                new SignalRMessage
                {

                    Target = "newMessage",
                    Arguments = new[] { JsonConvert.SerializeObject(signalAfterDataCreated).ToString() }
                });


        }



        [FunctionName("GetFloorWiseCount")]

        public static List<OperatorView> GetFloorWiseCount(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
           [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {         

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'Pending' and FromFloorId eq '" + data?.FromFloorId + "' and BuildingId eq '" + data?.BuildingId + "'").OrderByDesc("TimeStamp");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);

            var operatorViewData =
                   from personEntry in tableList.ToList()
                   group personEntry
                   by new { personEntry.BuildingId, personEntry.FromFloorId } into grp
                   orderby grp.Key.FromFloorId
                   select new OperatorView
                   {
                       BuildingId = grp.Key.BuildingId,
                       FromFloorId = grp.Key.FromFloorId,
                       Count = grp.Count(o => o.OperationStatus == "Pending")
                      
                   };
           
            return operatorViewData.ToList();
        }



        [FunctionName("GetWaitingStatusByBuildingId")]
        public static List<OperatorView> GetWaitingStatusByBuildingId(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus ne 'Completed'  and BuildingId eq '" + data?.BuildingId + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject).OrderBy(o=>o.Timestamp);

            var operatorViewData =
                   from personEntry in tableList.ToList()
                   group personEntry
                   by new { personEntry.BuildingId, personEntry.FromFloorId ,personEntry.OperatorId,personEntry.Direction} into grp
                   orderby grp.Key.FromFloorId
                   select new OperatorView
                   {
                       BuildingId = grp.Key.BuildingId,
                       FromFloorId = grp.Key.FromFloorId,
                       OperatorId= grp.Key.OperatorId,
                       Direction=grp.Key.Direction,
                       ElevatorStatus= grp.Select(o=>o.ElevatorStatus).First(),
                       OperatorStatus= grp.Select(o => o.OperationStatus).First(),
                       Count = grp.Count(),
                       ToFloorIds=grp.Select(o=>o.ToFloorId).ToList()
                   };

            return operatorViewData.ToList();
        }

        [FunctionName("GetElevatorStatus")]
        public static Object GetElevatorStatus(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            //TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'OnGoing'"+ "  and BuildingId eq '" + data?.BuildingId + "' and OperatorId eq '" + data?.OperatorId + "'" + " and FromFloorId eq '" + data?.FromFloorId + "'");
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'OnGoing'" + "  and BuildingId eq '" + data?.BuildingId + "'"+" and FromFloorId eq '" + data?.FromFloorId + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));

            var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject).OrderByDescending(o=>o.Timestamp);
          
            var operatorViewData =(
                from personEntry in tableList.ToList()
                group personEntry
                by new { personEntry.BuildingId, personEntry.FromFloorId, personEntry.OperatorId } into grp
                orderby grp.Key.FromFloorId
                select new 
                {
                    BuildingId = grp.Key.BuildingId,
                    FromFloorId = grp.Key.FromFloorId,
                    OperatorId = grp.Key.OperatorId,
                    ElevatorStatus = grp.Select(o => o.ElevatorStatus).First(),
                    OperatorStatus = grp.Select(o => o.OperationStatus).First()                   
                }).ToList();    

                return operatorViewData.Take(2);            
           
        }


        [FunctionName("UpdateWaitingStatus")]
        public static Task UpdateWaitingStatus(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {    

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());

            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'Pending' and FromFloorId eq '"+ data?.FromFloorId+"' and BuildingId eq '" + data?.BuildingId+ "' and Direction eq '" + data?.Direction+"'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
         
            List<PersonEntity> tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);
            tableList.ForEach(o => { o.OperationStatus = "OnGoing"; o.OperatorId = data?.OperatorId;o.ElevatorStatus = data?.ElevatorStatus; });

            tableList.ForEach(o => { AddNewPerson(cloudTable, o).Wait(); });     

            tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);

            var signalAfterWaitingStatusUpdate = new
            {
                operatorId = data?.OperatorId,
                fromFloorId = data?.FromFloorId,
                elevatorStatus=data?.ElevatorStatus,
                operationStatus="OnGoing",
                message= "UpdatedWaitingStatus"
            };

            return signalRMessages.AddAsync(
                    new SignalRMessage
                    {

                        Target = "newMessage",
                        Arguments = new[] { JsonConvert.SerializeObject(signalAfterWaitingStatusUpdate).ToString() }
                    });

        }



        [FunctionName("UpdateOnGoingStatus")]
        public static Task UpdateOnGoingStatus(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
        [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());

            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'OnGoing' and FromFloorId eq '" + data?.FromFloorId + "' and BuildingId eq '" + data?.BuildingId + "' and Direction eq '" + data?.Direction + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));

            List<PersonEntity> tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);
            tableList.ForEach(o => { o.OperationStatus = "Completed"; o.OperatorId = data?.OperatorId; o.ElevatorStatus = data?.ElevatorStatus; });

            tableList.ForEach(o => { AddNewPerson(cloudTable, o).Wait(); });

            tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);

            var signalAfterOnGoingStatusUpdate = new
            {
                operatorId = data?.OperatorId,
                fromFloorId = data?.FromFloorId,
                elevatorStatus = data?.ElevatorStatus,
                operationStatus = "Completed",
                message = "UpdatedOnGoingStatus"
            };

            return signalRMessages.AddAsync(
                new SignalRMessage
                {

                    Target = "newMessage",
                    Arguments = new[] { JsonConvert.SerializeObject(signalAfterOnGoingStatusUpdate).ToString() }
                });

        }


        [FunctionName("addToGroup")]
        public static Task AddToGroup(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")] HttpRequest req,
        ClaimsPrincipal claimsPrincipal,
        [SignalR(HubName = "chat")]
        IAsyncCollector<SignalRGroupAction> signalRGroupActions)
        {
            var userIdClaim = claimsPrincipal.FindFirst(ClaimTypes.NameIdentifier);
            return signalRGroupActions.AddAsync(
                new SignalRGroupAction
                {
                    UserId = userIdClaim.Value,
                    GroupName = "myGroup",
                    Action = GroupAction.Add
                });
        }






    }

}
