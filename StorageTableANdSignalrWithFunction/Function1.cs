using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Net.Http;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using FunctionApp1.Constraint;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace FunctionApp1
{
    public static class Function1
    {
        private static HttpClient httpClient = new HttpClient();

        private static string storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=elevatorstorage;AccountKey=ir3ksfG+Huw4ntCXJ8R6XCsYsuG+UjK7S5Wx+DN3XT48DPN6AW57DrZOrjKXsLNyyyL61jxPkLDNNRMlv3CDxA==;EndpointSuffix=core.windows.net";
        private static string tableName = "tblelevator";



        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            string responseMessage = string.IsNullOrEmpty(name)
                ? "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response."
                : $"Hello, {name}. This HTTP triggered function executed successfully.";


             return new OkObjectResult(responseMessage);
        }



        public static async Task AddNewPerson(CloudTable cloudTable, PersonEntity personEntity)
        {
            TableOperation tableOperation = TableOperation.InsertOrMerge(personEntity);
            TableResult tableResult = await cloudTable.ExecuteAsync(tableOperation);

            PersonEntity insertedPerson = tableResult.Result as PersonEntity;
            Console.WriteLine("Person Added Succesfully");
        }
   


        [FunctionName("index")]
        public static IActionResult GetHomePage([HttpTrigger(AuthorizationLevel.Anonymous)] HttpRequest req, ExecutionContext context)
        {
            var path = Path.Combine(context.FunctionAppDirectory, "content", "index.html");
            return new ContentResult
            {
                Content = File.ReadAllText(path),
                ContentType = "text/html",
            };
        }

        [FunctionName("negotiate")]
        public static SignalRConnectionInfo Negotiate(
            [HttpTrigger(AuthorizationLevel.Anonymous)] HttpRequest req,
            [SignalRConnectionInfo(HubName = "serverless")] SignalRConnectionInfo connectionInfo)
        {
            return connectionInfo;
        }


        //[FunctionName("broadcast")]
        //public static async Task Broadcast([TimerTrigger("*/25 * * * * *")] TimerInfo myTimer,
        //[SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        //{            
        //    await signalRMessages.AddAsync(
        //        new SignalRMessage
        //        {
        //            Target = "newMessage",
        //            Arguments = new[] { $"Signal time is : {DateTime.Now.ToString()}" }
        //        });
        //}

        public class PersonEntity : TableEntity
        {
            public PersonEntity() { }
            public PersonEntity(string firstName, string lastName)
            {
                PartitionKey = firstName;
                RowKey = lastName;
                Timestamp = DateTime.UtcNow;
            }    
            public string BuildingId { get; set; }
            public string FromFloorId { get; set; }
            public string ToFloorId { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime? UpdatedDate { get; set; }
            public string OperationStatus { get; set; }
            public string UpdateOperationId { get; set; }
            public string OperatorId { get; set; }
            public string CreatedBy { get; set; }
            public string ElevatorStatus { get; set; }

        }
        public class OperatorView
        {
            public string BuildingId { get; set; }
            public string FromFloorId { get; set; }
            public int Count { get; set; }
            public List<string> ToFloorIds { get; set; }
            public string OperatorId { get; set; }          
            public string ElevatorStatus { get; set; }

        }       



         [FunctionName("messages")]         
        public static Task SendMessage(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
            [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);          
           
            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());           
 
            PersonEntity person = new PersonEntity( Guid.NewGuid().ToString(), Guid.NewGuid().ToString())
            {
              BuildingId= data?.BuildingId,                
              FromFloorId= data?.FromFloorId,
              ToFloorId= data?.ToFloorId,
              CreatedDate=DateTime.UtcNow,
              OperationStatus= "pending",
              CreatedBy= "b7c2c5f4-d593-4187-ae02-bbac8cc41e4f",
              ElevatorStatus="",
              OperatorId=""

            };

            AddNewPerson(cloudTable, person).Wait();           

            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where( "OperationStatus eq 'pending'");

           string tableObject =JsonConvert.SerializeObject( cloudTable.ExecuteQuery(query));          
           var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);
   
            return signalRMessages.AddAsync(
                new SignalRMessage
                {

                    Target = "newMessage",
                  //  Arguments = new[] { DateTime.Now.ToString() }
                    Arguments = new[] {"Data Created" }
                });

        }



        [FunctionName("GetFloorWiseCount")]

        public static List<OperatorView> GetFloorWiseCount(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
           [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {         

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'pending' and FromFloorId eq '" + data?.FromFloorId + "' and BuildingId eq '" + data?.BuildingId + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);

            var operatorViewData =
                   from personEntry in tableList.ToList()
                   group personEntry
                   by new { personEntry.BuildingId, personEntry.FromFloorId } into grp
                   orderby grp.Key.FromFloorId
                   select new OperatorView
                   {
                       BuildingId = grp.Key.BuildingId,
                       FromFloorId = grp.Key.FromFloorId,
                       Count = grp.Count(o => o.OperationStatus == "pending"),
                       ToFloorIds=grp.Select(o=>o.ToFloorId).ToList()
                   };
           
            return operatorViewData.ToList();
        }



        [FunctionName("GetWaitingStatusByBuildingId")]
        public static List<OperatorView> GetWaitingStatusByBuildingId(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'pending'  and BuildingId eq '" + data?.BuildingId + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            var tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);

            var operatorViewData =
                   from personEntry in tableList.ToList()
                   group personEntry
                   by new { personEntry.BuildingId, personEntry.FromFloorId ,personEntry.OperatorId,personEntry.ElevatorStatus} into grp
                   orderby grp.Key.FromFloorId
                   select new OperatorView
                   {
                       BuildingId = grp.Key.BuildingId,
                       FromFloorId = grp.Key.FromFloorId,
                       OperatorId= grp.Key.OperatorId,
                       ElevatorStatus= grp.Key.ElevatorStatus,
                       Count = grp.Count(o => o.OperationStatus == "pending"),
                       ToFloorIds = grp.Select(o => o.ToFloorId).ToList()
                   };

            return operatorViewData.ToList();
        }

        [FunctionName("GetElevatorStatus")]
        public static Object GetElevatorStatus(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());
            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'completed'"+ "  and BuildingId eq '" + data?.BuildingId + "' and OperatorId eq '" + data?.OperatorId + "'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            var personEntry = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject).OrderByDescending(o=>o.Timestamp).FirstOrDefault();

            var operatorViewData =new                   
                   {
                       BuildingId = personEntry.BuildingId,
                       FromFloorId = personEntry.FromFloorId,
                       OperatorId = personEntry.OperatorId,
                       ElevatorStatus = personEntry.ElevatorStatus                    
                   };

            return operatorViewData;
        }


        [FunctionName("UpdateWaitingStatus")]
        public static Task UpdateWaitingStatus(
          [HttpTrigger(AuthorizationLevel.Anonymous, "post")] object message,
          [SignalR(HubName = "serverless")] IAsyncCollector<SignalRMessage> signalRMessages)
        {    

            CloudStorageAccount cloudStorageAccount;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable cloudTable = cloudTableClient.GetTableReference(tableName);

            PersonEntity data = JsonConvert.DeserializeObject<PersonEntity>(message.ToString());

            TableQuery<PersonEntity> query = new TableQuery<PersonEntity>().Where("OperationStatus eq 'pending' and FromFloorId eq '"+ data?.FromFloorId+"' and BuildingId eq '" + data?.BuildingId+"'");

            string tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
         
            List<PersonEntity> tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);
            tableList.ForEach(o => { o.OperationStatus = "completed"; o.OperatorId = data?.OperatorId;o.ElevatorStatus = data?.ElevatorStatus; });

            tableList.ForEach(o => { AddNewPerson(cloudTable, o).Wait(); });     

            tableObject = JsonConvert.SerializeObject(cloudTable.ExecuteQuery(query));
            tableList = JsonConvert.DeserializeObject<List<PersonEntity>>(tableObject);          

            return signalRMessages.AddAsync(
                    new SignalRMessage
                    {

                        Target = "newMessage",
                        Arguments = new[] { data?.OperatorId }
                    });

        }


        [FunctionName("addToGroup")]
        public static Task AddToGroup(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")] HttpRequest req,
        ClaimsPrincipal claimsPrincipal,
        [SignalR(HubName = "chat")]
        IAsyncCollector<SignalRGroupAction> signalRGroupActions)
        {
            var userIdClaim = claimsPrincipal.FindFirst(ClaimTypes.NameIdentifier);
            return signalRGroupActions.AddAsync(
                new SignalRGroupAction
                {
                    UserId = userIdClaim.Value,
                    GroupName = "myGroup",
                    Action = GroupAction.Add
                });
        }






    }

}
